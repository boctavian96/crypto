/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author octavian
 */
public abstract class AbstractWindow {
    
    private static final int MAIN_FRAME_WIDTH = 400;
    private static final int MAIN_FRAME_HEIGHT = 400;
    private static final int STATUS_LABEL_WIDTH = 350;
    private static final int STATUS_LABEL_HEIGHT = 100;
    
    
    protected JFrame mainFrame;
    protected JLabel headerLabel;
    protected JLabel statusLabel;
    protected JPanel controlPanel;
    
    public AbstractWindow(String title, int width, int height){
        prepareGUI(title, width, height);
    }
    
    public AbstractWindow(String title){
        this(title, MAIN_FRAME_WIDTH, MAIN_FRAME_HEIGHT);
    }
    
    
    private void prepareGUI(String formtext, int width, int height){
        mainFrame = new JFrame(formtext);
        mainFrame.setSize(width, height);
        mainFrame.setLayout(new GridLayout(3, 1));
        
        headerLabel = new JLabel("", JLabel.CENTER);
        statusLabel = new JLabel("", JLabel.CENTER);
        statusLabel.setSize(STATUS_LABEL_WIDTH, STATUS_LABEL_HEIGHT);
        
        mainFrame.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });
        
        controlPanel = new JPanel();
        controlPanel.setLayout(new FlowLayout());
        
        mainFrame.add(headerLabel);
        mainFrame.add(controlPanel);
        mainFrame.add(statusLabel);
        mainFrame.setVisible(true);
    }

}
