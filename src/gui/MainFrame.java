/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import algoritm.Algoritm;
import algoritm.aes.AES;
import algoritm.caesar.Caesar;
import algoritm.des.DES;
import algoritm.hill.Hill;
import algoritm.mars.Mars;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JTextField;

/**
 *
 * @author octavian
 */
public class MainFrame extends AbstractWindow{
    
    private DefaultComboBoxModel algorithmsName;
    private JComboBox algorithmsList;
    
    private JButton encryptButton;
    private JButton decryptButton;
    private JButton selectFileButton;
    private JButton exitButton;
    
    private JTextField publicKey;
    private JTextField privateKey;
    private JTextField filePath;
    
    private JFileChooser fileChooser;
    
    public MainFrame(String text){
        super(text);
    }
    
    public void showUserInterface(){

      encryptButton = new JButton("Encrypt");
      decryptButton = new JButton("Decrypt");
      selectFileButton = new JButton("Select");
      exitButton = new JButton("Exit");
      
      publicKey = new JTextField("publicKey");
      privateKey = new JTextField("privateKey");
      filePath = new JTextField("PathToFile");
      
      fileChooser = new JFileChooser();
      
      publicKey.setSize(300, 100);
      
        algorithmsName = new DefaultComboBoxModel();
        
        algorithmsName.addElement("RSA");
        algorithmsName.addElement("MARS");
        algorithmsName.addElement("Caesar");
        algorithmsName.addElement("Hill");
        algorithmsName.addElement("AES");
        algorithmsName.addElement("DES");
       
        algorithmsList = new JComboBox(algorithmsName);
        algorithmsList.setSelectedIndex(0);

      encryptButton.setActionCommand("Encrypt");
      decryptButton.setActionCommand("Decrypt");
      selectFileButton.setActionCommand("Select");
      exitButton.setActionCommand("Exit");

      encryptButton.addActionListener(new ButtonClickListener()); 
      decryptButton.addActionListener(new ButtonClickListener());
      selectFileButton.addActionListener((ActionEvent x) -> {
                int returnVal = fileChooser.showOpenDialog(mainFrame);
          
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    statusLabel.setText("File Selected :" + file.getName());
                    filePath.setText(file.getAbsolutePath());
                }
                });
      exitButton.addActionListener(new ButtonClickListener()); 
      algorithmsList.addActionListener(new ButtonClickListener());

      controlPanel.add(encryptButton);
      controlPanel.add(decryptButton);
      controlPanel.add(selectFileButton);
      controlPanel.add(exitButton);
      controlPanel.add(algorithmsList);
      
      controlPanel.add(publicKey);
      controlPanel.add(privateKey);
      controlPanel.add(filePath);
      
      mainFrame.setVisible(true);  
   }
    
    private class ButtonClickListener implements ActionListener{
      @Override
      public void actionPerformed(ActionEvent e) {
         String command = e.getActionCommand();
          Algoritm algoritm = null;
         
          switch (command) {
            case "Encrypt":
                int algorithmIndex = algorithmsList.getSelectedIndex();
                String encryptAlgoritm = (String)algorithmsList.getItemAt(algorithmIndex);
                switch(encryptAlgoritm){
                    case "AES":
                        algoritm = new AES(filePath.getText(), publicKey.getText());
                    break;
                    case "DES":
                        algoritm = new DES(filePath.getText(), publicKey.getText());
                    break;
                    case "Hill":
                        //algoritm = new Hill(command, key_matrix);
                    break;
                    case "Caesar":
                        algoritm = new Caesar(Integer.valueOf(publicKey.getText()), filePath.getText());
                    break;
                    case "Mars":
                        algoritm = new Mars(command, command, command);
                    break;
                    case "RSA":
                        //algoritm = new RSA();
                    break;
                        
                }
                break;
            case "Decrypt":
                int decalgorithmIndex = algorithmsList.getSelectedIndex();
                String decryptAlgoritm = (String)algorithmsList.getItemAt(decalgorithmIndex);
                //algoritm = decideAlgoritm(decryptAlgoritm);
                break;
              default:  	
                  System.exit(0);
          }
      }		
   }
    
}
