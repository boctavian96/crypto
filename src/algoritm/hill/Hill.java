/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.hill;

import filemanager.FReader;
import filemanager.FWriter;
import utils.MatrixUtils;
import utils.Utils;
import algoritm.Algoritm;

/**
 *
 * @author octavian
 */
public class Hill implements Algoritm {
    
    private String filename;
    private String text;
    private int[][] key;
    
    public Hill(String filename, int[][] key_matrix, String text){
        this.filename = filename;
        this.key = key_matrix;
        this.text = text;
    }
    
    public Hill(String filename, int[][] key_matrix){
        this.filename = filename;
        this.key = key_matrix;
    }
    
    private int[] generareTablou(char c1, char c2){
        int[] result = new int[2];
        
        result[0] = Utils.orderInAlphabet(c1);
        result[1] = Utils.orderInAlphabet(c2);
        
        return result;
    }
    
    private int[] generareCod(int[][] key, int[] matrix){
        int[] result;
        
        result = MatrixUtils.multiplicationMatrix(key, matrix);
        result = MatrixUtils.moduloMatrix(result, 26);
        
        return result;
    }
    
    private String codare(int[][] key, String text){
        StringBuilder result = new StringBuilder();
        char c1 = 0; 
        char c2 = 0;
        
        for(int i = 0; i < text.length(); i++){
            
            if(text.charAt(i) == 0){
                continue;
            }else{
                if(c1 == 0){
                    c1 = text.charAt(i);
                }else if(c2 == 0){
                    c2 = text.charAt(i);
                }
            }
            
            if(c1 != 0 && c2 != 0){
                int[] tablou = generareTablou(c1, c2);
                int[] cod = generareCod(key, tablou);
                
                result.append(Character.toString((char)(cod[0] + 65)));
                result.append(Character.toString((char)(cod[1] + 65)));
                
                c1 = 0;
                c2 = 0;
            }

        }
        
        return result.toString();
    }
    
    private int generateMultiplier(int det_key, int q){
        /*
        9*x mod 26 = 1
        9*x = 27
        x = 27/9
        x = 3
        */
        //
        return ((q*26) + 1)/det_key;
    }
    
    private int[][] generateDecodeKey(int[][] key){
        int[][] decodeKey;
        
        int determinant_key = MatrixUtils.determinantMatrice(key);
        
        int q = determinant_key / MatrixUtils.determinantMatrice(MatrixUtils.inversareMatrice(key));
        
        int multiplier = generateMultiplier(determinant_key, q);
        
        decodeKey = MatrixUtils.moduloMatrix(MatrixUtils.inversareMatrice(key), 26);
        
        decodeKey = MatrixUtils.multiplicationMatrix(decodeKey, multiplier);
        
        return MatrixUtils.moduloMatrix(decodeKey, 26);
    }
    
    private String decodare(int[][] matrix, String text){
        int[][] decode_key = generateDecodeKey(matrix);
        return codare(decode_key, text);
    }

    @Override
    public void encrypt() {
        FWriter fw = new FWriter(filename);
        fw.write(codare(key, text));
    }

    @Override
    public void decrypt() {
        FReader fr = new FReader(filename);
        this.text = fr.read();
        FWriter fw = new FWriter(Utils.getFilenameWithoutExtension(filename)+"_dec.txt");
        fw.write(decodare(key, this.text));
    }
    
}
