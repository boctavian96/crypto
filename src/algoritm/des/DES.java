/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.des;

import algoritm.Algoritm;
import filemanager.FReader;
import filemanager.FWriter;
import utils.Utils;

/**
 *
 * @author octavian
 */
public class DES implements Algoritm{
	// Implementam si triple des = > 3 sub chei
	private static byte[][] K;
	private static byte[][] K1;
	private static byte[][] K2;
        
        private final String filename;
        private final String key;
        private String input = null;

        public DES(String filename, String key, String input){
            this.filename = filename;
            this.key = key;
            this.input = input;
        }
        
        public DES(String filename, String key){
            this.filename = filename;
            this.key = key;
        }


	private static void setBit(byte[] data, int pos, int val) {
		int posByte = pos / 8;										
		int posBit = pos % 8;										
		byte tmpB = data[posByte];									
		tmpB = (byte) (((0xFF7F >> posBit) & tmpB) & 0x00FF);		
		byte newByte = (byte) ((val << (8 - (posBit + 1))) | tmpB);
		data[posByte] = newByte;
	}

	private static int extractBit(byte[] data, int pos) {
		int posByte = pos / 8;
		int posBit = pos % 8;										
		byte tmpB = data[posByte];
		int bit = tmpB >> (8 - (posBit + 1)) & 0x0001;
		return bit;
	}

	private static byte[] rotireStanga(byte[] input, int len, int pas) {
		int nrBytes = (len - 1) / 8 + 1;
		byte[] out = new byte[nrBytes];
		for (int i = 0; i < len; i++) {
			int val = extractBit(input, (i + pas) % len);
			setBit(out, i, val);
		}
		return out;
	}

	private static byte[] extractBits(byte[] input, int pos, int n) {
		int numOfBytes = (n - 1) / 8 + 1;							
		byte[] result = new byte[numOfBytes];						
		for (int i = 0; i < n; i++) {						
			int val = extractBit(input, pos + i);					
			setBit(result, i, val);
		}
		return result;

	}
	
	private static byte[] permutareFunctie(byte[] input, int[] table) {
		int nrBytes = (table.length - 1) / 8 + 1;
		byte[] result = new byte[nrBytes];
		for (int i = 0; i < table.length; i++) {
			int val = extractBit(input, table[i] - 1);
			setBit(result, i, val);
		}
		return result;

	}
	
	private static byte[] xor_func(byte[] a, byte[] b) {
		byte[] result = new byte[a.length];
		for (int i = 0; i < a.length; i++) {
			result[i] = (byte) (a[i] ^ b[i]);
		}
		return result;

	}
	
	private static byte[] encrypt64Bloc(byte[] bloc,byte[][] subkeys, boolean isDecrypt) {
		byte[] result = new byte[bloc.length];
		byte[] R = new byte[bloc.length / 2];
		byte[] L = new byte[bloc.length / 2];

		result = permutareFunctie(bloc, DESTables.IP);

		L = extractBits(result, 0, DESTables.IP.length/2);
		R = extractBits(result, DESTables.IP.length/2, DESTables.IP.length/2);

		for (int i = 0; i < 16; i++) {
			byte[] tmpR = R;
			if(isDecrypt)
				R = f_func(R, subkeys[15-i]);
			else
				R = f_func(R,subkeys[i]);
			R = xor_func(L, R);											
			L = tmpR;													
		}

		result = concatenareBiti(R, DESTables.IP.length/2, L, DESTables.IP.length/2);

		result = permutareFunctie(result, DESTables.invIP);
		return result;
	}

	private static byte[] f_func(byte[] R, byte[] K) {
		byte[] result;
		result = permutareFunctie(R, DESTables.expandTbl);
		result = xor_func(result, K);
		result = s_func(result);
		result = permutareFunctie(result, DESTables.P);
		return result;
	}

	private static byte[] s_func(byte[] in) {
		in = separareBiti(in, 6);
		byte[] result = new byte[in.length / 2];					
		int halfByte = 0;											
		for (int b = 0; b < in.length; b++) {
			byte valByte = in[b];
			int r = 2 * (valByte >> 7 & 0x0001) + (valByte >> 2 & 0x0001);														
			int c = valByte >> 3 & 0x000F;
			int val = DESTables.sboxes[b][r][c];
			if (b % 2 == 0)
				halfByte = val;
			else
				result[b / 2] = (byte) (16 * halfByte + val);
		}
		return result;
	}
	
	private static byte[] separareBiti(byte[] in, int len) {
		int numOfBytes = (8 * in.length - 1) / len + 1;	
		byte[] result = new byte[numOfBytes];
		for (int i = 0; i < numOfBytes; i++) {
			for (int j = 0; j < len; j++) {
				int val = extractBit(in, len * i + j);
				setBit(result, 8 * i + j, val);
			}
		}
		return result;
	}
	
	private static byte[] concatenareBiti(byte[] a, int aLen, byte[] b, int bLen) {
		int numOfBytes = (aLen + bLen - 1) / 8 + 1;
		byte[] result = new byte[numOfBytes];
		int j = 0;
		for (int i = 0; i < aLen; i++) {
			int val = extractBit(a, i);
			setBit(result, j, val);
			j++;
		}
		for (int i = 0; i < bLen; i++) {
			int val = extractBit(b, i);
			setBit(result, j, val);
			j++;
		}
		return result;
	}
	
	
	private static byte[] deletePadding(byte[] input) {
		int count = 0;

		int i = input.length - 1;
		while (input[i] == 0) {
			count++;
			i--;
		}

		byte[] result = new byte[input.length - count - 1];
		System.arraycopy(input, 0, result, 0, result.length);
		return result;
	}

	
	
	private static byte[][] generareSubChei(byte[] key) {
		byte[][] result = new byte[16][];						
		byte[] auxK = permutareFunctie(key, DESTables.PC1);

		byte[] C = extractBits(auxK, 0, DESTables.PC1.length/2);
		byte[] D = extractBits(auxK, DESTables.PC1.length/2, DESTables.PC1.length/2);

		for (int i = 0; i < 16; i++) {

			C = rotireStanga(C, 28, DESTables.keyShift[i]);
			D = rotireStanga(D, 28, DESTables.keyShift[i]);

			byte[] cd = concatenareBiti(C, 28, D, 28);

			result[i] = permutareFunctie(cd, DESTables.PC2);
		}

		return result;
	}
	
	public static byte[] codare(byte[] data, byte[] key) {
		int lenght=0;
		byte[] padding = new byte[1];
		int i;
		lenght = 8 - data.length % 8;
		padding = new byte[lenght];
		padding[0] = (byte) 0x80;
		
		for (i = 1; i < lenght; i++)
			padding[i] = 0;

		byte[] result = new byte[data.length + lenght];
		byte[] bloc = new byte[8];

		K = generareSubChei(key);
		
		int count = 0;

		for (i = 0; i < data.length + lenght; i++) {
			if (i > 0 && i % 8 == 0) {
				bloc = encrypt64Bloc(bloc,K, false);
				System.arraycopy(bloc, 0, result, i - 8, bloc.length);
			}
			if (i < data.length)
				bloc[i % 8] = data[i];
			else{														
				bloc[i % 8] = padding[count % 8];
				count++;
			}
		}
		if(bloc.length == 8){
			bloc = encrypt64Bloc(bloc,K, false);
			System.arraycopy(bloc, 0, result, i - 8, bloc.length);
		}
		return result;
	}
	
	public static byte[] TripleDES_codare(byte[] data,byte[][] keys)
	{
		int lenght=0;
		byte[] padding = new byte[1];
		int i;

		lenght = 8 - data.length % 8;
		padding = new byte[lenght];
		padding[0] = (byte) 0x80;
	
		for (i = 1; i < lenght; i++)
			padding[i] = 0;


		byte[] result = new byte[data.length + lenght];
		byte[] bloc = new byte[8];
		

		K = generareSubChei(keys[0]);
		K1 = generareSubChei(keys[1]);
		K2 = generareSubChei(keys[2]);
		
		int count = 0;

		for (i = 0; i < data.length + lenght; i++) {
			if (i > 0 && i % 8 == 0) {
				bloc = encrypt64Bloc(bloc,K, false);						
				bloc = encrypt64Bloc(bloc,K1, true);		
				bloc = encrypt64Bloc(bloc,K2, false);
				System.arraycopy(bloc, 0, result, i - 8, bloc.length);
			}
			if (i < data.length)
				bloc[i % 8] = data[i];
			else {														
				bloc[i % 8] = padding[count % 8];
				count++;
			}
		}
		if(bloc.length == 8){
			bloc = encrypt64Bloc(bloc,K, false);
			bloc = encrypt64Bloc(bloc,K1, true);
			bloc = encrypt64Bloc(bloc,K2, false);
			System.arraycopy(bloc, 0, result, i - 8, bloc.length);
		}
		return result;
	}
	
	
	public static byte[] TripleDES_decodare(byte[] data,byte[][] keys)
	{
		int i;
		byte[] result = new byte[data.length];
		byte[] bloc = new byte[8];

		K = generareSubChei(keys[0]);
		K1 = generareSubChei(keys[1]);
		K2 = generareSubChei(keys[2]);

		for (i = 0; i < data.length; i++) {
			if (i > 0 && i % 8 == 0) {
				bloc = encrypt64Bloc(bloc,K2, true);
				bloc = encrypt64Bloc(bloc,K1, false);
				bloc = encrypt64Bloc(bloc,K, true);
				System.arraycopy(bloc, 0, result, i - 8, bloc.length);
			}
			if (i < data.length)
				bloc[i % 8] = data[i];
		}
		bloc = encrypt64Bloc(bloc,K2, true);
		bloc = encrypt64Bloc(bloc,K1, false);
		bloc = encrypt64Bloc(bloc,K, true);
		System.arraycopy(bloc, 0, result, i - 8, bloc.length);

		
		result = deletePadding(result);

		return result;
	}

	
	public static byte[] decodare(byte[] data, byte[] key) {
		int i;
		byte[] result = new byte[data.length];
		byte[] bloc = new byte[8];
		
		K = generareSubChei(key);

		for (i = 0; i < data.length; i++) {
			if (i > 0 && i % 8 == 0) {
				bloc = encrypt64Bloc(bloc,K, true);
				System.arraycopy(bloc, 0, result, i - 8, bloc.length);
			}
			if (i < data.length)
				bloc[i % 8] = data[i];
		}
		bloc = encrypt64Bloc(bloc,K, true);
		System.arraycopy(bloc, 0, result, i - 8, bloc.length);


		result = deletePadding(result);

		return result;
	}

    @Override
    public void encrypt() {
        FWriter fw = new FWriter(filename);
        fw.write(codare(input.getBytes(), key.getBytes()));
    }

    @Override
    public void decrypt() {
        FReader fr = new FReader(filename);
        FWriter fw = new FWriter(Utils.getFilenameWithoutExtension(filename) + "_dec" + ".txt");
        byte[] b = fr.readBytes();
        for(int i = 0; i < b.length; i++){
            System.out.print(b[i] + " ");
        }
        fw.write(decodare(fr.readBytes(), key.getBytes()));
    }
}
