/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm;

/**
 *
 * @author octavian
 */
public interface Algoritm {
    /**
     * Functie in care se realizeaza cripatarea si se scrie fisierul criptat
     */
    public void encrypt();
    /**
     * Functie in care se realizeaza decriptarea si se scrie fisierul decriptat
     */
    public void decrypt();
}
