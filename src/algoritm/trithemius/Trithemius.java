/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.trithemius;

import filemanager.FReader;
import filemanager.FWriter;
import java.util.Arrays;
import utils.Utils;
import algoritm.Algoritm;

/**
 *
 * @author octavian
 */
public class Trithemius implements Algoritm {
	private char[][] matrice = new char[26][26];
	private String text;
	private String cheie;
	private char[] codif;
        private final String filename;
      
        public Trithemius(String text, String cheie, String filename){
		this.text = text.toUpperCase();
		this.cheie = cheie.toUpperCase();
		this.codif = new char[text.length()];
                this.filename = filename;
	}
	
	public char [][] getMatrice(){
		return matrice;
	}
	
	private void generareMatrice(){
		for(int i = 0;i<26;i++){
			matrice[0][i] = (char)('A'+i);
			matrice[i][0] = (char)('A'+i);
		}
		for(int i = 1;i<26;i++)
			for(int j = 1;j<26;j++){
				matrice[i][j] = (char)((matrice[i][j-1]+1-'A')%26+'A');
			}
	}
	
	public void printMatrice(){
		for(int i = 0;i<26;i++){
			for(int j = 0;j<26;j++)
				System.out.print(matrice[i][j]+" ");
			System.out.println();
		}
	}
	
	public char searchMatrice(char t,char c){
		int linie = 0;
		int coloana = 0;
		for(int i = 0;i<26;i++){
			if(matrice[i][0] == t)
				linie = i;
			if(matrice[0][i] == c)
				coloana = i;
		}
		return matrice[linie][coloana];
			
	}
	
	private void codificare(){
		int k = 0;
                generareMatrice();
		for(int i = 0;i<text.length();i++){
			codif[i] = text.charAt(i);
			if(text.charAt(i) != ' '){
				if(k == cheie.length())
					k = 0;
				codif[i] = searchMatrice(text.charAt(i),cheie.charAt(k));
				k++;
			}
		}
		System.out.println("Textul codificat: ");
		System.out.println(codif);
		System.out.println();
	}
	
	public char decodeMatrice(char t,char c){
		int col = c-'A';
		for(int i = 1;i<26;i++)
			if(matrice[i][col] == t)
				return matrice[i][0];
		return 'A';
	}
	
	private char[] decodificare(){
		int k = 0;
                StringBuilder sb = new StringBuilder();
                generareMatrice();
		System.out.println("Textul decodificat:");
		for(int i = 0;i<codif.length;i++)
			if(codif[i] != ' '){
				if(k == cheie.length())
					k = 0;
				System.out.print(decodeMatrice(codif[i],cheie.charAt(k)));
                                sb.append(decodeMatrice(codif[i],cheie.charAt(k)));
				k++;
			}
			else
				System.out.print(" ");
		System.out.println();
                
                return sb.toString().toCharArray();
        }
        
        @Override
        public void encrypt(){
            codificare();
            FWriter fw = new FWriter(filename);
            String result;
            result = String.copyValueOf(codif);
            System.out.println("Value of codif: " + result);
            fw.write(result);
        }
        
        
        @Override 
        public void decrypt(){
            FReader fr = new FReader(filename);
            String input = fr.read();
            codif = input.toCharArray();
            char[] result = decodificare();
            FWriter fw = new FWriter(Utils.getFilenameWithoutExtension(filename) + "_dec.txt");
            fw.write(result);
        }

}

