/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.playfair;

/**
 *
 * @author octavian
 */
public class CharInfo {
    private char i; //Caracterul 
    private int x; //Pozitia x in tablou
    private int y; //Pozitia y in tablou

    public CharInfo(char i, int x, int y) {
        this.i = i;
        this.x = x;
        this.y = y;
    }

    public char getI() {
        return i;
    }

    public void setI(char i) {
        this.i = i;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    

}
