/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.playfair;

import filemanager.FReader;
import filemanager.FWriter;
import java.util.ArrayList;
import java.util.List;
import utils.Utils;
import algoritm.Algoritm;


//TODO Update THIS
/**
 *
 * @author octavian
 */
public class PlayFair implements Algoritm {
    private static final int VALUE = 5;
    private static final String LETTERS = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
    private static final char SPECIAL_CHAR = 'Q';
    private char[][] tablou = new char[VALUE][VALUE];
    private String filename;
    private String pass;
    private String args;
       
    public PlayFair(String pass, String args, String filename){
        this.pass = pass.toUpperCase();
        this.args = processInput(args);
        this.filename = filename;
        generateMatrix();
    }
    
    public PlayFair(String pass, String filename){
        this.pass = pass.toUpperCase();
        FReader fr = new FReader(filename);
        this.args = processInput(fr.read());
        this.filename = filename;
    }
    
    private void generateMatrix(){
        for(char i : pass.toCharArray()){
            if(i == 'i' || i == 'I'){
                if(!exists(tablou, 'J')){
                    insertInTable('J');
                }
            }else{
                if(!exists(tablou, i)){
                    insertInTable(i);
                }
                }
        }
        
        char[] letters = LETTERS.toCharArray();
        int i = 0;
        while(Utils.tableFreeSpace(tablou, VALUE) > 0){
            if(!exists(tablou, letters[i])){
                insertInTable(letters[i]);
            }
            i++;
        }
        
    }
    
    private void insertInTable(char x){
        boolean wasInserted = false;
        for(int i = 0; i < VALUE; i++){
            for(int j = 0; j < VALUE; j++){
                if(tablou[i][j] == 0 && !wasInserted){
                    tablou[i][j] = x;
                    wasInserted = true;
                }
            }
        }
    }
    
    private boolean exists(char[][] matrix, char x){
        for(int i = 0; i < VALUE; i++){
            for(int j = 0; j < VALUE; j++){
                if(matrix[i][j] == x && x != ' '){
                    return true;
                }
            }
        }
        return false;
    }
    
    public String codare(){
        generateMatrix();
        StringBuilder result = new StringBuilder();
        int x1;
        int x2;
        int y1;
        int y2;
        List<Pair> pairs;
        if(!Utils.isEven(args.length())){
            args = args.concat(String.valueOf(SPECIAL_CHAR));
        }
        pairs = generatePairs(args);
        
        for(Pair i : pairs){
            x1 = i.c1.getX();
            y1 = i.c1.getY();
            x2 = i.c2.getX();
            y2 = i.c2.getY();
            
            if(i.sameCol()){
                //Regula 1
                if(x1 == 4){
                    x1 = 0;
                }else{
                    x1++;
                }
                if(x2 == 4){
                    x2 = 0;
                }else{
                    x2++;
                }               
            }else if(i.sameRow()){
                //Regula 2
                if(y1 == 4){
                    y1 = 0;
                }else{
                    y1++;
                }
                if(y2 == 4){
                    y2 = 0;
                }else{
                    y2++;
                }
                

            }else{
                //Regula 3
                int aux = y1;
                y1 = y2;
                y2 = aux;
            }
        
            result.append(tablou[x1][y1]);
            result.append(tablou[x2][y2]);
        }
        
        return result.toString();
    }
    
    public void decodare(){
        
    }
    
    private List<Pair> generatePairs(String args){
        List<Pair> pairs = new ArrayList<>();
        CharInfo c1;
        CharInfo c2;
        for(int i = 0; i < args.length(); i+=2){
            char a = args.charAt(i);
            char b = args.charAt(i+1);
            
            if(a == 'I'){
                a = 'J';
            }
            
            if(b == 'I'){
                b = 'J';
            }

            
            c1 = new CharInfo(a, Utils.getElementX(tablou, a), Utils.getElementY(tablou, a));
            c2 = new CharInfo(b, Utils.getElementX(tablou, b), Utils.getElementY(tablou, b));
            pairs.add(new Pair(c1, c2));
        }
        
        return pairs;
    }
    
    private String processInput(String x){
        StringBuilder sb = new StringBuilder();
        
        for(int i = 0; i < x.length(); i++){
            if(x.charAt(i) == ' '){
                sb.append(SPECIAL_CHAR);
            }else{
                sb.append(x.charAt(i));
            }
        }
        
        return sb.toString();
    }

    @Override
    public void encrypt() {
        FWriter fw = new FWriter(filename);
        fw.write(codare());
    }

    @Override
    public void decrypt() {
        FReader fr = new FReader(filename);
        String input = fr.read();
        FWriter fw = new FWriter(Utils.getFilenameWithoutExtension(filename)+"_dec.txt");
        fw.write(codare());
    }
}