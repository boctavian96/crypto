/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.playfair;

/**
 *
 * @author octavian
 */
public class Pair {
    CharInfo c1; 
    CharInfo c2;

    public Pair(CharInfo c1, CharInfo c2) {
        this.c1 = c1;
        this.c2 = c2;
    }
    
    public boolean sameRow(){
        return c1.getX() == c2.getX();
    }
    
    public boolean sameCol(){
        return c1.getY() == c2.getY();
    }
    
}
