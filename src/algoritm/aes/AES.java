/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.aes;

import algoritm.Algoritm;
import filemanager.FReader;
import filemanager.FWriter;
import utils.Utils;

/**
 *
 * @author octavian
 */
public class AES implements Algoritm{
    	private static int Nb, Nk, Nr;
	private static byte[][] w;
        private final String filename;
        private final String key;
        private String input = null;
        
        public AES(String filename, String key, String input){
            this.filename = filename;
            this.key = key;
            this.input = input;
        }
        
        public AES(String filename, String key){
            this.filename = filename;
            this.key = key;
        }

	private static byte[] xor_func(byte[] a, byte[] b) {
		byte[] out = new byte[a.length];
		for (int i = 0; i < a.length; i++) {
			out[i] = (byte) (a[i] ^ b[i]);
		}
		return out;

	}

	private static byte[][] generateSubkeys(byte[] key) {
		byte[][] tmp = new byte[Nb * (Nr + 1)][4];

		int i = 0;
		while (i < Nk) {

			tmp[i][0] = key[i * 4];
			tmp[i][1] = key[i * 4 + 1];
			tmp[i][2] = key[i * 4 + 2];
			tmp[i][3] = key[i * 4 + 3];
			i++;
		}
		i = Nk;
		while (i < Nb * (Nr + 1)) {
			byte[] temp = new byte[4];
			for(int k = 0;k<4;k++)
				temp[k] = tmp[i-1][k];
			if (i % Nk == 0) {
				temp = SubWord(rotare(temp));
				temp[0] = (byte) (temp[0] ^ (AESTables.Rcon[i / Nk] & 0xff));
			} else if (Nk > 6 && i % Nk == 4) {
				temp = SubWord(temp);
			}
			tmp[i] = xor_func(tmp[i - Nk], temp);
			i++;
		}

		return tmp;
	}

	private static byte[] SubWord(byte[] in) {
		byte[] tmp = new byte[in.length];

		for (int i = 0; i < tmp.length; i++)
			tmp[i] = (byte) (AESTables.sbox[in[i] & 0x000000ff] & 0xff);

		return tmp;
	}

	private static byte[] rotare(byte[] input) {
		byte[] tmp = new byte[input.length];
		tmp[0] = input[1];
		tmp[1] = input[2];
		tmp[2] = input[3];
		tmp[3] = input[0];

		return tmp;
	}

	private static byte[][] AddRoundKey(byte[][] state, byte[][] w, int round) {

		byte[][] tmp = new byte[state.length][state[0].length];

		for (int c = 0; c < Nb; c++) {
			for (int l = 0; l < 4; l++)
				tmp[l][c] = (byte) (state[l][c] ^ w[round * Nb + c][l]);
		}

		return tmp;
	}

	private static byte[][] SubBytes(byte[][] state) {

		byte[][] tmp = new byte[state.length][state[0].length];
		for (int row = 0; row < 4; row++)
			for (int col = 0; col < Nb; col++)
				tmp[row][col] = (byte) (AESTables.sbox[(state[row][col] & 0x000000ff)] & 0xff);

		return tmp;
	}
	private static byte[][] InvSubBytes(byte[][] state) {
		for (int row = 0; row < 4; row++) 
			for (int col = 0; col < Nb; col++)
				state[row][col] = (byte)(AESTables.inv_sbox[(state[row][col] & 0x000000ff)]&0xff);
		
		return state;
	}

	private static byte[][] ShiftRows(byte[][] state) {

		byte[] t = new byte[4];
		for (int r = 1; r < 4; r++) {
			for (int c = 0; c < Nb; c++)
				t[c] = state[r][(c + r) % Nb];
			for (int c = 0; c < Nb; c++)
				state[r][c] = t[c];
		}

		return state;
	}
	
	private static byte[][] InvShiftRows(byte[][] state) { 
		byte[] t = new byte[4]; 
		for (int r = 1; r < 4; r++) {
			for (int c = 0; c < Nb; c++) 
				t[(c + r)%Nb] = state[r][c];
			for (int c = 0; c < Nb; c++) 
				state[r][c] = t[c];
		}
	return state;
	}

	private static byte[][] InvMixColumns(byte[][] s){
		 int[] sp = new int[4];
	      byte b02 = (byte)0x0e, b03 = (byte)0x0b, b04 = (byte)0x0d, b05 = (byte)0x09;
	      for (int c = 0; c < 4; c++) {
	         sp[0] = FFMul(b02, s[0][c]) ^ FFMul(b03, s[1][c]) ^ FFMul(b04,s[2][c])  ^ FFMul(b05,s[3][c]);
	         sp[1] = FFMul(b05, s[0][c]) ^ FFMul(b02, s[1][c]) ^ FFMul(b03,s[2][c])  ^ FFMul(b04,s[3][c]);
	         sp[2] = FFMul(b04, s[0][c]) ^ FFMul(b05, s[1][c]) ^ FFMul(b02,s[2][c])  ^ FFMul(b03,s[3][c]);
	         sp[3] = FFMul(b03, s[0][c]) ^ FFMul(b04, s[1][c]) ^ FFMul(b05,s[2][c])  ^ FFMul(b02,s[3][c]);
	         for (int i = 0; i < 4; i++) s[i][c] = (byte)(sp[i]);
	      }
	      
	      return s;
	}
	
	private static byte[][] MixColumns(byte[][] s){
		 int[] sp = new int[4];
	      byte b02 = (byte)0x02, b03 = (byte)0x03;
	      for (int c = 0; c < 4; c++) {
	         sp[0] = FFMul(b02, s[0][c]) ^ FFMul(b03, s[1][c]) ^ s[2][c]  ^ s[3][c];
	         sp[1] = s[0][c]  ^ FFMul(b02, s[1][c]) ^ FFMul(b03, s[2][c]) ^ s[3][c];
	         sp[2] = s[0][c]  ^ s[1][c]  ^ FFMul(b02, s[2][c]) ^ FFMul(b03, s[3][c]);
	         sp[3] = FFMul(b03, s[0][c]) ^ s[1][c]  ^ s[2][c]  ^ FFMul(b02, s[3][c]);
	         for (int i = 0; i < 4; i++) s[i][c] = (byte)(sp[i]);
	      }
	      
	      return s;
	}

	public static byte FFMul(byte a, byte b) {
		byte aa = a, bb = b, r = 0, t;
		while (aa != 0) {
			if ((aa & 1) != 0)
				r = (byte) (r ^ bb);
			t = (byte) (bb & 0x80);
			bb = (byte) (bb << 1);
			if (t != 0)
				bb = (byte) (bb ^ 0x1b);
			aa = (byte) ((aa & 0xff) >> 1);
		}
		return r;
	}

	public static byte[] encryptBloc(byte[] in) {
		byte[] tmp = new byte[in.length];
		
		

		byte[][] state = new byte[4][Nb];

		for (int i = 0; i < in.length; i++)
			state[i / 4][i % 4] = in[i%4*4+i/4];

		state = AddRoundKey(state, w, 0);
		for (int round = 1; round < Nr; round++) {
			state = SubBytes(state);
			state = ShiftRows(state);
			state = MixColumns(state);
			state = AddRoundKey(state, w, round);
		}
		state = SubBytes(state);
		state = ShiftRows(state);
		state = AddRoundKey(state, w, Nr);

		for (int i = 0; i < tmp.length; i++)
			tmp[i%4*4+i/4] = state[i / 4][i%4];

		return tmp;
	}

	public static byte[] decryptBloc(byte[] in) {
		byte[] tmp = new byte[in.length];

		byte[][] state = new byte[4][Nb];

		for (int i = 0; i < in.length; i++)
			state[i / 4][i % 4] = in[i%4*4+i/4];

		state = AddRoundKey(state, w, Nr);
		for (int round = Nr-1; round >=1; round--) {
			state = InvSubBytes(state);
			state = InvShiftRows(state);
			state = AddRoundKey(state, w, round);
			state = InvMixColumns(state);
			
		}
		state = InvSubBytes(state);
		state = InvShiftRows(state);
		state = AddRoundKey(state, w, 0);

		for (int i = 0; i < tmp.length; i++)
			tmp[i%4*4+i/4] = state[i / 4][i%4];

		return tmp;
	}
	
	public static byte[] codare(byte[] in,byte[] key){
		
		Nb = 4;
		Nk = key.length/4;
		Nr = Nk + 6;
		
		
		int lenght=0;
		byte[] padding = new byte[1];
		int i;
		lenght = 16 - in.length % 16;				
		padding = new byte[lenght];					
		padding[0] = (byte) 0x80;
		
		for (i = 1; i < lenght; i++)				
			padding[i] = 0;

		byte[] result = new byte[in.length + lenght];		
		byte[] bloc = new byte[16];							
		
		
		w = generateSubkeys(key);
		
		int count = 0;

		for (i = 0; i < in.length + lenght; i++) {
			if (i > 0 && i % 16 == 0) {
				bloc = encryptBloc(bloc);
				System.arraycopy(bloc, 0, result, i - 16, bloc.length);
			}
			if (i < in.length)
				bloc[i % 16] = in[i];
			else{														
				bloc[i % 16] = padding[count % 16];
				count++;
			}
		}
		if(bloc.length == 16){
			bloc = encryptBloc(bloc);
			System.arraycopy(bloc, 0, result, i - 16, bloc.length);
		}
		
		return result;
	}
	
	public static byte[] decodare(byte[] in,byte[] key){
		int i;
		byte[] result = new byte[in.length];
		byte[] bloc = new byte[16];
		
		
		Nb = 4;
		Nk = key.length/4;
		Nr = Nk + 6;
		w = generateSubkeys(key);


		for (i = 0; i < in.length; i++) {
			if (i > 0 && i % 16 == 0) {
				bloc = decryptBloc(bloc);
				System.arraycopy(bloc, 0, result, i - 16, bloc.length);
			}
			if (i < in.length)
				bloc[i % 16] = in[i];
		}
		bloc = decryptBloc(bloc);
		System.arraycopy(bloc, 0, result, i - 16, bloc.length);


		result = deletePadding(result);

		return result;
	}
	
	private static byte[] deletePadding(byte[] input) {
		int count = 0;

		int i = input.length - 1;
		while (input[i] == 0) {
			count++;
			i--;
		}

		byte[] tmp = new byte[input.length - count - 1];
		System.arraycopy(input, 0, tmp, 0, tmp.length);
		return tmp;
	}

    @Override
    public void encrypt() {
        FWriter fw = new FWriter(filename);
        fw.write(codare(input.getBytes(), key.getBytes()));
    }

    @Override
    public void decrypt() {
        FReader fr = new FReader(filename);
        FWriter fw = new FWriter(Utils.getFilenameWithoutExtension(filename) + "_dec" + ".txt");
        fw.write(decodare(fr.readBytes(), key.getBytes()));
    }
	
}
