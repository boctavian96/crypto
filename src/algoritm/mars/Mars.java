package algoritm.mars;

import algoritm.Algoritm;
import filemanager.FReader;
import filemanager.FWriter;

public class Mars implements Algoritm{
	
	private static int[] K;
        private String filename;
        private final String key;
        private final String input;
        
        public Mars(String filename, String key, String input){
            this.filename = filename;
            this.key = key;
            this.input = input;
        }
        
        public Mars(String filename, String key){
            this.filename = filename;
            this.key = key;
            input = "LOL";
        }
	
	private static int rotl(int val, int pas) {
		return (val << pas) | (val >>> (32 - pas));
	}
	private static int rotr(int val, int pas) {
		return (val >>> pas) | (val << (32-pas));
	}
	
	
	private static int[] expandKey(byte[] key){
		int n = key.length/4;
		int[] tmp = new int[40];
		int[] data = new int[n];
		
		for(int i =0;i<data.length;i++)
			data[i] = 0;
		
		int off = 0;
		for(int i=0;i<data.length;i++){
			data[i] = 	((key[off++]&0xff))|
						((key[off++]&0xff) << 8) |
						((key[off++]&0xff) << 16) |
						((key[off++]&0xff) << 24);
		}
		
		int[] T = new int[15];
		for(int i=0;i<T.length;i++){
			if(i<data.length) T[i] = data[i];
			else if(i == data.length) T[i] = n;
			else T[i] = 0;
		}
		
		for(int j=0;j<4;j++){
			for(int i=0;i<T.length;i++)
				T[i] = T[i] ^ (rotl(T[Math.abs(i-7 %15)] ^ T[Math.abs(i-2 %15)],3) ^ (4*i+j));
			for(int c=0;c<4;c++)
				for(int i=0;i<T.length;i++)
					T[i] = T[i] + rotl(MarsTables.s_box[(int)(T[Math.abs(i-1%15)] & 0x000001ff)],9);
			for(int i = 0;i<=9;i++) tmp[10*j+i] = T[4*i%15];
		}
		
		int[] B = {0xa4a8d57b, 0x5b5d193b, 0xc8a8309b, 0x73f9a978};
		int j,w,m,r,p;
		for(int i = 5;i<=35;i++){
			j = tmp[i] & 0x00000003;
			w = tmp[i] | 0x00000003;
			m = generateMask(w);
			r = tmp[i-1] & 0x0000001f;
			p = rotl(B[j],r);
			tmp[i] = w ^ (p & m); 
		}
		
		return tmp;
	}
	
	private static int generateMask(int x){
		int m;
		
		m = (~x ^ (x>>>1)) & 0x7fffffff;
		m &= (m >> 1) & (m >> 2); 
		m &= (m >> 3) & (m >> 6); 
		    
		if(m == 0) 
			return 0;

		m <<= 1; m |= (m << 1); m |= (m << 2); m |= (m << 4);

		m |= (m << 1) & ~x & 0x80000000;

		return m & 0xfffffffc;

	}
	
	
	public static byte[] encryptBloc(byte[] in){
		byte[] tmp =  new byte[in.length];
		int aux;

		
		int[] data = new int[in.length/4];
		for(int i =0;i<data.length;i++)
			data[i] = 0;
		int off = 0;
		for(int i=0;i<data.length;i++){
			data[i] = 	((in[off++]&0xff))|
						((in[off++]&0xff) << 8) |
						((in[off++]&0xff) << 16) |
						((in[off++]&0xff) << 24);
		}
	
		int A = data[0],B = data[1],C = data[2],D = data[3];
		A = A + K[0];
		B = B + K[1];
		C = C + K[2];
		D = D + K[3];
		
		//forward mixing
		for(int i = 0;i<=7;i++){
			B = B ^ MarsTables.s_box[A & 0xff];
			B = B + MarsTables.s_box[(rotr(A,8) & 0xff) + 256]; 
            C = C + MarsTables.s_box[rotr(A,16) & 0xff]; 
            D = D ^ MarsTables.s_box[(rotr(A,24) & 0xff) + 256]; 
            
            A = rotr(A,24);
            
            if(i == 1 || i == 5) A = A + B;
            if(i == 0 || i == 4) A = A + D;
            
            aux = A;
			A = B;
			B = C;
			C = D;
			D = aux;
		}
		int R,L,M;
		int[] eout;
		//cryptographic core
		for(int i = 0;i<=15;i++){
			
			eout = E_func(A, K[2*i+4], K[2*i+5]);
			
			A = rotl(A,13);
			C = C + eout[1];
			
			if(i<8) {
				B = B + eout[0];
				D = D ^ eout[2];
			}
			else{
				D = D + eout[0];
				B = B ^ eout[2];
			}
			
			aux = A;
			A = B;
			B = C;
			C = D;
			D = aux;
		}
		//backward mixing
		for(int i = 0;i<=7;i++){
			
			if(i == 3 || i == 7) A = A - B;
			if(i == 2 || i == 6) A = A - D;
			
			B = B ^ MarsTables.s_box[(A & 0xff) + 256];
			C = C - MarsTables.s_box[rotr(A,24) & 0xff];
			D = D - MarsTables.s_box[(rotr(A,16) & 0xff) + 256];
			D = D ^ MarsTables.s_box[rotr(A,8) & 0xff];
			
			A = rotl(A,24);
			
			aux = A;
			A = B;
			B = C;
			C = D;
			D = aux;

		}
		A = A - K[36];
		B = B - K[37];
		C = C - K[38];
		D = D - K[39];
		
		data[0] = A;data[1] = B;data[2] = C;data[3] = D;
		
		for(int i = 0;i<tmp.length;i++){
			tmp[i] = (byte)((data[i/4] >>> (i%4)*8) & 0xff);
		}
		
		return tmp;	
	}
	
	private static int[] E_func(int in,int k1,int k2){
		int[] tmp = new int[3];
		int M,L,R;
		M = in + k1;
		R = rotl(in,13) * k2;
		L = MarsTables.s_box[M & 0x000001ff];
		R = rotl(R,5);
		M = rotl(M,R & 0x0000001f);
		L = L ^ R;
		R = rotl(R,5);
		L = L ^ R;
		L = rotl(L,R & 0x0000001f);
		
		tmp[0] = L;
		tmp[1] = M;
		tmp[2] = R;
		
		return tmp;
	}
	
	public static byte[] decryptBloc(byte[] in){
		byte[] tmp =  new byte[in.length];
		int aux;
		
		
		int[] data = new int[in.length/4];
		for(int i =0;i<data.length;i++)
			data[i] = 0;
		int off = 0;
		for(int i=0;i<data.length;i++){
			data[i] = 	((in[off++]&0xff))|
						((in[off++]&0xff) << 8) |
						((in[off++]&0xff) << 16) |
						((in[off++]&0xff) << 24);
		}
	
		int A = data[0],B = data[1],C = data[2],D = data[3];
		A = A + K[36];
		B = B + K[37];
		C = C + K[38];
		D = D + K[39];
		
		//forward mixing
		for(int i = 7;i>=0;i--){
			
			aux = D;
			D = C;
			C = B;
			B = A;
			A = aux;
			
			A = rotr(A,24);
			
			D = D ^ MarsTables.s_box[(rotr(A,8) & 0xff)];
			D = D + MarsTables.s_box[(rotr(A,16) & 0xff) + 256];
			C = C + MarsTables.s_box[rotr(A,24) & 0xff];
			B = B ^ MarsTables.s_box[(A & 0xff) + 256];         
            
            if(i == 2 || i == 6) A = A + D;
            if(i == 3 || i == 7) A = A + B;
            
		}
		
		int[] eout;
		// cryptographic core
		for(int i = 15;i>=0;i--){
			
			aux = D;
			D = C;
			C = B;
			B = A;
			A = aux;
			
			A = rotr(A,13);
			eout = E_func(A, K[2*i+4], K[2*i+5]);
			
			C = C - eout[1];
			
			if(i<8) {
				B = B - eout[0];
				D = D ^ eout[2];
			}
			else{
				D = D - eout[0];
				B = B ^ eout[2];
			}

		}
		//backward mixing
		for(int i = 7;i>=0;i--){
			
			aux = D;
			D = C;
			C = B;
			B = A;
			A = aux;
			
			if(i == 0 || i == 4) A = A - D;
			if(i == 1 || i == 5) A = A - B;
			
			A = rotl(A,24);
			
			D = D ^ MarsTables.s_box[(rotr(A,24) & 0xff) + 256];
			C = C - MarsTables.s_box[rotr(A,16) & 0xff];
			B = B - MarsTables.s_box[(rotr(A,8) & 0xff) + 256];
			B = B ^ MarsTables.s_box[A & 0xff];
			

		}
		A = A - K[0];
		B = B - K[1];
		C = C - K[2];
		D = D - K[3];
		
		data[0] = A;data[1] = B;data[2] = C;data[3] = D;
		
		for(int i = 0;i<tmp.length;i++){
			tmp[i] = (byte)((data[i/4] >>> (i%4)*8) & 0xff);
		}
		
		return tmp;	
	}
	
	public static byte[] codare(byte[] in,byte[] key){
		K = expandKey(key);
		int lenght=0;
		byte[] padding = new byte[1];
		int i;
		lenght = 16 - in.length % 16;				
		padding = new byte[lenght];					
		padding[0] = (byte) 0x80;
		
		for (i = 1; i < lenght; i++)				
			padding[i] = 0;

		byte[] tmp = new byte[in.length + lenght];		
		byte[] bloc = new byte[16];	
		
		int count = 0;

		for (i = 0; i < in.length + lenght; i++) {
			if (i > 0 && i % 16 == 0) {
				bloc = encryptBloc(bloc);
				System.arraycopy(bloc, 0, tmp, i - 16, bloc.length);
			}
			if (i < in.length)
				bloc[i % 16] = in[i];
			else{														
				bloc[i % 16] = padding[count % 16];
				count++;
			}
		}
		if(bloc.length == 16){
			bloc = encryptBloc(bloc);
			System.arraycopy(bloc, 0, tmp, i - 16, bloc.length);
		}
		
		return tmp;
	}
	
	public static byte[] decodare(byte[] in,byte[] key){
		byte[] tmp = new byte[in.length];
		byte[] bloc = new byte[16];
		K = expandKey(key);
		int i;
		for (i = 0; i < in.length; i++) {
			if (i > 0 && i % 16 == 0) {
				bloc = decryptBloc(bloc);
				System.arraycopy(bloc, 0, tmp, i - 16, bloc.length);
			}
			if (i < in.length)
				bloc[i % 16] = in[i];
		}
		bloc = decryptBloc(bloc);
		System.arraycopy(bloc, 0, tmp, i - 16, bloc.length);


		tmp = deletePadding(tmp);
		
		return tmp;
	}
	
	private static byte[] deletePadding(byte[] input) {
		int count = 0;

		int i = input.length - 1;
		while (input[i] == 0) {
			count++;
			i--;
		}

		byte[] tmp = new byte[input.length - count - 1];
		System.arraycopy(input, 0, tmp, 0, tmp.length);
		return tmp;
	}

    @Override
    public void encrypt() {
        FWriter fw = new FWriter(filename);
        fw.write(codare(input.getBytes(), key.getBytes()));
    }

    @Override
    public void decrypt() {
        FReader fr = new FReader(filename);
        FWriter fw = new FWriter(filename);
        
        String input = fr.read();
       // fw.write(decodare(in, key));
    }
}
