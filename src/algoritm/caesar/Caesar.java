/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.caesar;

import filemanager.*;
import utils.Utils;
import algoritm.Algoritm;

/**
 *
 * @author octavian
 */
public class Caesar implements Algoritm{
    
    private String filename;
    private String input;
    private StringBuilder output;
    private int key;
    
    private static final String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    public Caesar(int key, String filename){
        this.filename = filename;
        this.key = key;
    }
    
    public Caesar(String input, int key, String filename){
        this.input = input;
        this.filename = filename;
        this.key = key;
    }
    
    @Override
    public void encrypt(){
        FWriter f = new FWriter(filename);
        f.write(algorithm(key));
    }
    
    @Override
    public void decrypt(){
        FReader f = new FReader(filename);
        input = f.read();
        
        FWriter fw = new FWriter(Utils.getFilenameWithoutExtension(filename) + "_dec.txt");
        fw.write(algorithm(-key));
    }
    
    /**
     * Functie folosita pentru a verifica rezultatele in consola
     * @param key
     * @return 
     */
    public String algoConsoleDebug(int key){
        return algorithm(key);
    }
    
    private String algorithm(int key){
        this.output = new StringBuilder();
        
        for(char i : input.toCharArray()){
            output.append((char)(i + key));
            //output.append((char)(letters.indexOf(key)%26)); 
        }
        
        return output.toString();
    }
    
    
}
