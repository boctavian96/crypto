package algoritm.rsa;

import java.math.BigInteger;
/**
 * Interface providing often used BigInteger constants.
 * 
 * @author octavian
 */

public interface RSAConfig
{
    BigInteger ZERO = BigInteger.ZERO;
    BigInteger ONE = BigInteger.ONE;
    BigInteger TWO = BigInteger.valueOf(2);
    BigInteger THREE = BigInteger.valueOf(3);
    BigInteger TWO_FIFTY_SIX = BigInteger.valueOf(256);
}