/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritm.rsa;

import algoritm.Algoritm;
import filemanager.FReader;
import java.io.File;
import utils.Utils;


/**
 *
 * @author octavian
 */
public class RSA implements Algoritm{
    	private static final int SUCCESS = 0;
	private static final int ERROR = 1;
	private static final int INCORRECT_INVOCATION = 127;
        
        private final RSAKeyGenerator keygen;
	private final RSACompleteKey completeKey;
	private final RSAPublicKey publicKey;
	private final RSAPrivateKey privateKey;
        
        private final String filePath;
        // File paths.  Only the file specified in plaintext must exist.
	private	String plaintext = null;
	private	String cryptotext = null;
	private	String decryptedText1 = null;
	private	String decryptedText2 = null;
                
        private int status;
        
        public RSA(String filepath){
            this.filePath = filepath;
            this.cryptotext = Utils.getFilenameWithoutExtension(filepath) + "_dec.txt";
            this.keygen = new RSAKeyGenerator();
            this.completeKey = (RSACompleteKey)keygen.makeKey(RSAKey.COMPLETE_KEY);
            this.publicKey = (RSAPublicKey)keygen.makeKey(RSAKey.PUBLIC_KEY);
            this.privateKey = (RSAPrivateKey)keygen.makeKey(RSAKey.PRIVATE_KEY);
        
        }
        
    public void readFile(){
        FReader fr = new FReader(filePath);
        this.plaintext = fr.read();
    }
        
    private void codare(){
                readFile();

		// Encryption using the public key.  Other tests rely on this part working,
		if (!publicKey.use(plaintext, cryptotext)) {
			System.out.println("Public key encryption failed.");
			File f = new File(plaintext);
			System.out.println(String.format("plaintext from: %s", f.getAbsolutePath()));
		}
		
		status = SUCCESS;
    }
    
    private void decodare(){
        	// Decryption using the private key.
		if (!privateKey.use(cryptotext, decryptedText1)) {
			System.out.println("Private key decryption failed.");
			status = ERROR;
		}
				
		// Decryption using the complete key.
		if (!completeKey.use(cryptotext, decryptedText2)) {
			System.out.println("Complete key decryption failed.");
			status = ERROR;
		}
    }
        
    @Override
    public void encrypt() {
        codare();
    }

    @Override
    public void decrypt() {
        decodare();
    }
}

