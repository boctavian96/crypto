/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author octavian
 */
public class MatrixUtils {
    /**
     * Sum of two matrices
     * @param matrix_a
     * @param matrix_b
     * @return 
     */
    public static int[][] sumMatrix(int[][] matrix_a, int[][] matrix_b){
        int[][] result = new int[matrix_a.length][matrix_a.length];
        
        for(int i = 0; i < matrix_a.length; i++){
            for(int j = 0; j < matrix_a.length; i++){
                result[i][j] = matrix_a[i][j] + matrix_b[i][j];
            }
        }
       
        return result;
    }
    
    /**
     * 
     * @param matrix
     * @param n
     * @return 
     */
    public static int[][] multiplicationMatrix(int[][] matrix, int n){
        int[][] result = new int[matrix.length][matrix.length];
        for(int i=0; i< matrix.length; i++){
            for(int j=0; j < matrix.length; j++){
                result[i][j] = matrix[i][j] * n;
            }
        }
        
        return result;
    }
    
    /**
     * Multiplication of two matrices
     * @param matrix
     * @param matrix2
     * @return 
     */
    public static int[] multiplicationMatrix(int[][] matrix, int[] matrix2){
        int[] result = new int[matrix2.length];
        
        for(int i=0; i< matrix.length; i++){
            for(int j=0; j < matrix.length; j++){
                result[i] += matrix[i][j] * matrix2[j];
            }
        }
        
        return result;
    }
    
    /**
     * Modulo of a matrix
     * @param matrix
     * @param modulo
     * @return 
     */
    public static int[] moduloMatrix(int[] matrix, int modulo){
        int[] result = new int[matrix.length];
        
        for(int i = 0; i < matrix.length; i++){
            if(matrix[i] < 0){
                result[i] = modulo - matrix[i] % modulo;
            }else{
                result[i] = matrix[i] % modulo;
            }
        }
        
        return result;
    }
    
    public static int[][] moduloMatrix(int[][] matrix, int modulo){
        int[][] result = new int[matrix.length][matrix.length];
        
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix.length; j++){
                if(matrix[i][j] < 0){
                   //(-x) % y = y - x % y
                   result[i][j] = modulo - (-matrix[i][j] % modulo);
                }else{
                    result[i][j] = matrix[i][j] % modulo;
                }
            }
        }
        
        return result;
    }
    
    public static int[][] inversareMatrice(int[][] key){
        int[][] result = new int[key.length][key.length];
        
        result[0][0] = key[1][1];
        result[0][1] = - key[0][1];
        result[1][0] = - key[1][0];    
        result[1][1] = key[0][0];
        
        return result;
    }
    
    public static int determinantMatrice(int[][] matrix){
        return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
    }
}
