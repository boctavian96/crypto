/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import algoritm.hill.Hill;

/**
 *
 * @author octavian
 */
public class HillTest {
        public static void main(String[] args){
        /*
        |3   3|
        |2   5|
        */
        int[][] matrix = new int[2][2];
        matrix[0][0] = 3;
        matrix[0][1] = 3;
        matrix[1][0] = 2;
        matrix[1][1] = 5;
        
        
        Hill h = new Hill("Hill.txt", matrix, "HATS");
        h.encrypt();
        
        
        Hill h2 = new Hill("Hill.txt", matrix);
        h.decrypt();
    }
}
