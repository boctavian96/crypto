/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import algoritm.des.DES;

/**
 *
 * @author octavian
 */
public class DESTest {
    public static void main(String[] args) {
        DES des = new DES("des_test.txt", "5qw8sd4h", "Algoritmul de criptare DES");
        byte[] cod = des.codare("Algoritmul de criptare DES".getBytes(), "5qw8sd4h".getBytes());
        des.encrypt();
        
        byte[] decode = des.decodare(cod, "5qw8sd4h".getBytes());
        System.out.println(new String(decode));
        
        for(int i = 0; i < cod.length; i++){
             System.out.print(cod[i] + " ");
        }
        
        System.out.println();
        
       
        des.decrypt();
        
    }
}
