/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import algoritm.caesar.Caesar;

/**
 *
 * @author octavian
 */
public class CaesarTest {
    
    public static final String FILE_NAME = "caesar1.txt";
    public static final String TEST_INPUT = "JAJS NS HWDUYTQLD XNQJSHJ NX LTQIJSXXXX";
                                           //EVEN CRYPTOLOGY SILENCE IS GOLDNESSSS  
    public static final String TEST_ANA = "Ana are mere";
    
    public static void main(String[] args){
        System.out.println("Textul initial: " + TEST_INPUT);
        encr_test(0);
        //encr_test(15);
        
        //encr_test(1);
        decr_test(1);
        
        //encr_test(2);
        decr_test(2);
        
        //encr_test(3);
        decr_test(3);
        
        //encr_test(4);
        decr_test(4);
        
        //encr_test(5);
        decr_test(5);
        
        //encr_test(6);
        decr_test(6);
    }
    
    private static void encr_test(int key){
        Caesar c = new Caesar(TEST_INPUT, key, FILE_NAME);
        c.encrypt();
        System.out.println("Textul criptat : " + c.algoConsoleDebug(key));
    }
    
    private static void decr_test(int key){
        Caesar c = new Caesar(key, FILE_NAME);
        c.decrypt();
        System.out.println("Textul decriptat : " + c.algoConsoleDebug(-key) + "\n");
    } 
}
